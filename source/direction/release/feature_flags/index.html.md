---
layout: markdown_page
title: "Category Vision - Feature Flags"
---

- TOC
{:toc}

## Feature Flags

A feature flag is a technique in SW development that enables a feature to be tested even before it is completed and ready for release. 
Feature flag is used to hide, enable or disable the feature during run time. 
The technique allows developers to release a version of a product that has unfinished features. 
These unfinished features are hidden (toggled) so they do not appear in the user interface. 
This allows many small incremental versions of software to be delivered without the cost of constant branching and merging. 

Feature flags unlock faster, more agile delivery workflows by providing
control over the flexibility of  deployment to a specific environment or audience. In addition it reduces risk to production, in case a problem is detected, 
disabling the feature is as easy as turning off a switch. 

Feature Flags is built with an [Unleash](https://github.com/Unleash/unleash)-compatible
 API, ensuring interoperability with any other compatible tooling,
 and taking advantage of the various client libraries available for
 Unleash.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AFeature%20Flags)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/user/project/operations/feature_flags.html)
- [Vision Deep Dive](https://www.youtube.com/watch?v=U8ks-EggZMI&feature=youtu.be)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1295) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Now that we have released feature flags that support two different strategies,
 % rollout ([gitlab#8240](https://gitlab.com/gitlab-org/gitlab/issues/8240)) and userID ([gitlab#11459](https://gitlab.com/gitlab-org/gitlab/issues/11459)),
we are moving forward with using feature flags internally as part of our deployment process ([gitlab#26842](https://gitlab.com/gitlab-org/gitlab/issues/26842)).

In addition, we are working on a powerful integration between feature flags and the issues and/or merge requests
that are affected by them. Since GitLab serves as a single application  tool, users can now associate feature flags 
directly form the issue or merge request and vice versa and view the current deployment status
from any location ([gitlab#26456](https://gitlab.com/gitlab-org/gitlab/issues/26456)). 


## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)).

We currently have basic capabilities and want to continue and extend these in the upcoming releases. 

Key deliverables to achieve this are:

- [% rollout for Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/8240) (Complete)
- [UserID-based access](https://gitlab.com/gitlab-org/gitlab/issues/11459) (Complete)
- [Add ability to associate feature flag with contextual issue/epic/MR](https://gitlab.com/gitlab-org/gitlab/issues/26456)
- [Feature Flag Gradual Rollout strategy based on groups](https://gitlab.com/gitlab-org/gitlab/issues/13308)
- [Cookie-based access](https://gitlab.com/gitlab-org/gitlab/issues/11456)
- [API/CDN caching for feature flags](https://gitlab.com/gitlab-org/gitlab/issues/9479)
- [More explicit logging for accesses](https://gitlab.com/gitlab-org/gitlab/issues/9157)
- [A/B testing based on Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/34813)
- [Add Rule based Feature Flag rollout strategy support](https://gitlab.com/gitlab-org/gitlab/issues/33315)
- [Permissions for Feature Flags](https://gitlab.com/gitlab-org/gitlab/issues/8239)
- [Feature Flags user metrics](https://gitlab.com/gitlab-org/gitlab/issues/31442)

## Competitive Landscape

Other feature flag products offer more comprehensive targeting and
configuration. The simplicity of our solution is actually a strength
compared to this in some cases, but there is some basic functionality
still to add. As we are rigorously working to close the gaps with the competitors, our next strategy to tackle will be the ability to configure feature flags based on groups [gitlab-ee#13308](https://gitlab.com/gitlab-org/gitlab-ee/issues/13308)

There is a detailed LaunchDarkly comparison from when the project
was first being conceived [here](https://docs.google.com/spreadsheets/d/1p3QhVvdL7-RCD2pd8mm5a5q38D958a-vwPPWnBT4pmE/edit#gid=0).

## Analyst Landscape

Analysts are recognizing that this sort of capability is becoming
more a part of what's fundamentally needed for a continuous delivery
platform, in order to minimize blast radius from changes. Often,
solutions in this space are complex and hard to get up and running
with, and they are not typically bundled or well integrated with CD
solutions. It's also unclear how to get started.

This backs up our desire to not overcomplicated the solution space
here, and highlights the need for guidance. [gitlab#9450](https://gitlab.com/gitlab-org/gitlab/issues/9450)
introduces new in-product documentation to help development and
operations teams learn how to successfully adopt feature flags.

## Top Customer Success/Sales Issue(s)

None yet, but feedback is welcome.

## Top Customer Issue(s)

Being able to manage feature flags from an environment view [gitlab#9098](https://gitlab.com/gitlab-org/gitlab/issues/9098)

## Top Internal Customer Issue(s)

- Being able to control permissions ([gitlab#8239](https://gitlab.com/gitlab-org/gitlab/issues/8239))
on a per-environment basis for feature flags is a key driver of adoption
for our own production usage.
- Enabling Unleash in our codebase to enable internal use of our feture flag solution ([gitlab-ee#57943](https://gitlab.com/gitlab-org/gitlab-ce/issues/57943))

### Delivery Team

- [framework#64](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/64) enables dashboard annotations for feature flags
- Feature Flags: [framework#216](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/216) and [framework#32](https://gitlab.com/gitlab-com/gl-infra/delivery/issues/32)

## Top Vision Item(s)

Our top vision item is to [Natively support hypercloud deployments](https://gitlab.com/groups/gitlab-org/-/epics/1804), we want to help make it easier and quicker to get started and deploy to any one of the big cloud providers using GitLab's CI/CD..
